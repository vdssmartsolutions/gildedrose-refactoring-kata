package com.gildedrose.item;


import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SulfurasTest {
    @Test
    public void updateQualityThenQualityAndSellInShouldNotChange() {
        Sulfuras sulfuras = Sulfuras.builder()
                .quality(30)
                .sellInNumberOfDays(10)
                .build();

        sulfuras.updateQuality();

        assertThat(sulfuras.getQuality()).isEqualTo(30);
        assertThat(sulfuras.getSellInNumberOfDays()).isEqualTo(10);
    }
}
