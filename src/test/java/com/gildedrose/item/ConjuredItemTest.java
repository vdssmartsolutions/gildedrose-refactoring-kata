package com.gildedrose.item;


import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ConjuredItemTest {
    @Test
    public void updateQualityWithQualityAboveZeroAndSellInAboveZeroThenQualityShouldDecreaseBy2() {
        ConjuredItem conjuredItem = ConjuredItem.builder()
                .name("conjured-item")
                .quality(30)
                .sellInNumberOfDays(10)
                .build();

        conjuredItem.updateQuality();

        assertThat(conjuredItem.getQuality()).isEqualTo(28);
    }

    @Test
    public void updateQualityWithQualityAboveZeroAndSellInEqualToZeroThenQualityShouldDecreaseBy4() {
        ConjuredItem conjuredItem = ConjuredItem.builder()
                .name("conjured-item")
                .quality(30)
                .sellInNumberOfDays(0)
                .build();

        conjuredItem.updateQuality();

        assertThat(conjuredItem.getQuality()).isEqualTo(26);
    }
}
