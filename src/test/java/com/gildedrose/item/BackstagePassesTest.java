package com.gildedrose.item;


import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class BackstagePassesTest {
    @Test
    public void updateQualityWithQualityBelow50AndSellInAbove11ThenQualityShouldGoUpBy2() {
        BackstagePasses backstagePasses = BackstagePasses.builder()
                .sellInNumberOfDays(12)
                .quality(30)
                .build();

        backstagePasses.updateQuality();

        assertThat(backstagePasses.getQuality()).isEqualTo(31);
    }

    @Test
    public void updateQualityWithQualityBelow50AndSellInBelow11ThenQualityShouldGoUpBy2() {
        BackstagePasses backstagePasses = BackstagePasses.builder()
                .sellInNumberOfDays(10)
                .quality(30)
                .build();

        backstagePasses.updateQuality();

        assertThat(backstagePasses.getQuality()).isEqualTo(32);
    }

    @Test
    public void updateQualityWithQualityBelow50AndSellInBelow6ThenQualityShouldGoUpBy3() {
        BackstagePasses backstagePasses = BackstagePasses.builder()
                .sellInNumberOfDays(5)
                .quality(30)
                .build();

        backstagePasses.updateQuality();

        assertThat(backstagePasses.getQuality()).isEqualTo(33);
    }

    @Test
    public void updateQualityWithSellInBelow1ThenQualityShouldGoResetToZero() {
        BackstagePasses backstagePasses = BackstagePasses.builder()
                .sellInNumberOfDays(0)
                .quality(30)
                .build();

        backstagePasses.updateQuality();

        assertThat(backstagePasses.getQuality()).isEqualTo(0);
    }
}