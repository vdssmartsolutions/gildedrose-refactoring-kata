package com.gildedrose.item;


import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class RegularItemTest {
    @Test
    public void updateQualityWithQualityAboveZeroAndSellInAboveZeroThenQualityShouldDecreaseBy1() {
        RegularItem regularItem = RegularItem.builder()
                .name("other")
                .quality(30)
                .sellInNumberOfDays(10)
                .build();

        regularItem.updateQuality();

        assertThat(regularItem.getQuality()).isEqualTo(29);
    }

    @Test
    public void updateQualityWithQualityZeroAndSellInAboveZeroThenQualityShouldNotDecrease() {
        RegularItem regularItem = RegularItem.builder()
                .name("other")
                .quality(0)
                .sellInNumberOfDays(10)
                .build();

        regularItem.updateQuality();

        assertThat(regularItem.getQuality()).isEqualTo(0);
    }

    @Test
    public void updateQualityWithQualityAboveZeroAndSellInEqualToZeroThenQualityShouldDecreaseBy2() {
        RegularItem regularItem = RegularItem.builder()
                .name("other")
                .quality(30)
                .sellInNumberOfDays(0)
                .build();


        regularItem.updateQuality();

        assertThat(regularItem.getQuality()).isEqualTo(28);
    }

    @Test
    public void updateQualityForOtherItemsWithQualityAboveZeroAndSellInBelowZeroThenQualityShouldDecreaseBy2() {
        RegularItem regularItem = RegularItem.builder()
                .name("other")
                .quality(30)
                .sellInNumberOfDays(-1)
                .build();

        regularItem.updateQuality();

        assertThat(regularItem.getQuality()).isEqualTo(28);
    }


    @Test
    public void updateQualityThenSellInShouldDecreaseBy1() {
        RegularItem regularItem = RegularItem.builder()
                .name("other")
                .quality(30)
                .sellInNumberOfDays(10)
                .build();

        regularItem.updateQuality();

        assertThat(regularItem.getSellInNumberOfDays()).isEqualTo(9);
    }
}
