package com.gildedrose.item;


import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AgedBrieTest {
    @Test
    public void updateQualityWithQualityAbove50AndSellInAboveZero() {
        AgedBrie agedBrie = AgedBrie.builder()
                .sellInNumberOfDays(1)
                .quality(50)
                .build();

        agedBrie.updateQuality();

        assertThat(agedBrie.getName()).isEqualTo("Aged Brie");
        assertThat(agedBrie.getQuality()).isEqualTo(50);
        assertThat(agedBrie.getSellInNumberOfDays()).isEqualTo(0);
    }

    @Test
    public void updateQualityWithQualityBelow50AndSellInAboveZeroThenQualityShouldGoUpBy1() {
        AgedBrie agedBrie = AgedBrie.builder()
                .sellInNumberOfDays(1)
                .quality(10)
                .build();

        agedBrie.updateQuality();

        assertThat(agedBrie.getQuality()).isEqualTo(11);
    }

    @Test
    public void updateQualityWithQualityBelow50AndSellInLowerOrEqualToZeroThenQualityShouldGoUpBy2() {
        AgedBrie agedBrie = AgedBrie.builder()
                .sellInNumberOfDays(0)
                .quality(10)
                .build();

        agedBrie.updateQuality();

        assertThat(agedBrie.getQuality()).isEqualTo(12);
    }
}
