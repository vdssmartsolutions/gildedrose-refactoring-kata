package com.gildedrose;


import com.gildedrose.item.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class GildedRoseTest {
    @Mock
    private Item item;
    @Mock
    private Item item2;

    @Test
    public void updateQuality() {
        GildedRose gildedRose = GildedRose.builder()
                .items(List.of(item, item2))
                .build();

        gildedRose.updateQuality();

        verify(item).updateQuality();
        verify(item2).updateQuality();
    }
}
