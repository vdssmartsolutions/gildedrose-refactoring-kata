package com.gildedrose;

import com.gildedrose.item.*;

import java.util.stream.IntStream;

public class TexttestFixture {
    public static void main(String[] args) {
        System.out.println("OMGHAI!");

        GildedRose app = GildedRose.builder()
                .item(RegularItem.builder().name("+5 Dexterity Vest").sellInNumberOfDays(10).quality(20).build())
                .item(AgedBrie.builder().sellInNumberOfDays(2).quality(0).build())
                .item(RegularItem.builder().name("Elixir of the Mongoose").sellInNumberOfDays(5).quality(7).build())
                .item(Sulfuras.builder().sellInNumberOfDays(0).quality(80).build())
                .item(Sulfuras.builder().sellInNumberOfDays(-1).quality(80).build())
                .item(BackstagePasses.builder().sellInNumberOfDays(15).quality(20).build())
                .item(BackstagePasses.builder().sellInNumberOfDays(10).quality(49).build())
                .item(BackstagePasses.builder().sellInNumberOfDays(5).quality(49).build())
                .item(ConjuredItem.builder().name("Conjured Mana Cake").sellInNumberOfDays(3).quality(6).build())
                .build();

        int days = 10;
        if (args.length > 0) {
            days = Integer.parseInt(args[0]) + 1;
        }

        IntStream.range(0, days)
                .forEach(day -> {
                    System.out.println("-------- day " + day + " --------");
                    System.out.println("name, sellIn, quality");
                    for (Item item : app.getItems()) {
                        System.out.println(item);
                    }
                    System.out.println();
                    app.updateQuality();
                });
    }

}
