package com.gildedrose;

import com.gildedrose.item.Item;
import com.gildedrose.item.RegularItem;
import lombok.Builder;
import lombok.Getter;
import lombok.Singular;

import java.util.List;
import java.util.stream.Stream;

@Getter
@Builder
class GildedRose {
    @Singular
    private final List<Item> items;

    public void updateQuality() {
        items.forEach(Item::updateQuality);
    }
}