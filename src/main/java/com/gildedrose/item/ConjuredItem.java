package com.gildedrose.item;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@Builder
public class ConjuredItem implements Item {
    private String name;
    private int sellInNumberOfDays;
    private int quality;

    @Override
    public void updateQuality() {
        incrementQuality(getSellInNumberOfDays() < 1 ? -4 : -2);
        decrementSellInNumberOfDays();
    }
}
