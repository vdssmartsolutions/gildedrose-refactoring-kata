package com.gildedrose.item;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@Builder
public class Sulfuras implements Item {
    private static final String NAME = "Sulfuras, Hand of Ragnaros";

    private String name;
    private int sellInNumberOfDays;
    private int quality;

    public String getName() {
        return NAME;
    }

    @Override
    public void updateQuality() {
    }
}
