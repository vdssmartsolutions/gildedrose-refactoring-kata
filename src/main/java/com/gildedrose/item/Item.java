package com.gildedrose.item;

public interface Item {
    int MAX_QUALITY = 50;
    int MIN_QUALITY = 0;

    int getQuality();
    void setQuality(int value);

    int getSellInNumberOfDays();
    void setSellInNumberOfDays(int days);

    void updateQuality();

    default void decrementSellInNumberOfDays() {
        setSellInNumberOfDays(getSellInNumberOfDays() - 1);
    }

    default void incrementQuality(int value) {

        int qualityValue = getQuality() + value;

        if (qualityValue < MIN_QUALITY) {
            qualityValue = MIN_QUALITY;
        } else if (qualityValue > MAX_QUALITY) {
            qualityValue = MAX_QUALITY;
        }

        setQuality(qualityValue);
    }

    default void resetQuality() {
        setQuality(0);
    }
}
