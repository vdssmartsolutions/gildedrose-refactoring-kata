package com.gildedrose.item;

import lombok.*;

@Data
@ToString
@Builder
public class RegularItem implements Item {
    private static final int MAX_QUALITY = 50;
    private static final int MIN_QUALITY = 0;

    private String name;
    private int sellInNumberOfDays;
    private int quality;

    @Override
    public void updateQuality() {
        incrementQuality(this.sellInNumberOfDays < 1 ? -2 : -1);
        decrementSellInNumberOfDays();
    }
}
