package com.gildedrose.item;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@Builder
public class AgedBrie implements Item {
    private static final String NAME = "Aged Brie";

    private String name;
    private int sellInNumberOfDays;
    private int quality;

    public String getName() {
        return NAME;
    }

    @Override
    public void updateQuality() {
        incrementQuality(getSellInNumberOfDays() < 1 ? 2 : 1);
        decrementSellInNumberOfDays();
    }
}
