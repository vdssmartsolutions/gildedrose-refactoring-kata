package com.gildedrose.item;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@Builder
public class BackstagePasses implements Item {
    private static final String NAME = "Backstage passes to a TAFKAL80ETC concert";

    private String name;
    private int sellInNumberOfDays;
    private int quality;

    public String getName() {
        return NAME;
    }

    @Override
    public void updateQuality() {
        if (getSellInNumberOfDays() > 10) {
            incrementQuality(1);
        } else if (getSellInNumberOfDays() > 5) {
            incrementQuality(2);
        } else if (getSellInNumberOfDays() > 0) {
            incrementQuality(3);
        } else {
            resetQuality();
        }

        decrementSellInNumberOfDays();
    }
}
