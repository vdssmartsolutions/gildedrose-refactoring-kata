# GildedRose-Refactoring-Kata

## About
This is my humble attempt to the refactoring exercise that can be found on https://github.com/emilybache/GildedRose-Refactoring-Kata.

## Refactor Process
1. Writing unit tests capturing all requirements on the original code
2. For the unit tests I also added assertJ as dependencies as they have a more readable and clean API.
3. Added proper encapsulation on the Item class (Sorry goblin ;-), I await your fury but I couldn't let this one slide.)
4. Added lombok library to avoid boilerplate java code and introduced some handy builders.
5. Clean up code: 
    - Moving logic to domain methods like addQuality, resetQuality on Item
    - Defining named constants for the item names.
    - Using java streams, getting rid of the for loop.
    - Inverting logic to more reflect the description of the requirements.
6. Split up the logic for the different item types in seperate methods (untangling the logic)
7. Moved out the update sell in logic.
8. Used inheritance to move the specific update logic to their specific domain objects (DDD).
9. Added a item feature package
10. Finally implementing the new feature for Conjured Items was super easy by just adding a new ConjuredItem class.

## Refactoring Continued (2022)
1. Split up unit tests GildedRoseTest to separate test classes to make them truly unit tests
2. Introduced an Item interface and a RegularItem implementation
3. Renamed sellIn field to sellInNumberOfDays for readability
4. Upgraded to java 11
5. Defined lib versions under properties in pom file
6. Upgraded to junit 5 and added mockito

## How to run
1. Install Maven
2. Run "mvn clean install" on the root folder (this will also download the needed dependencies)
3. Run TexttestFixture.java in the "src\test\java\com\gildedrose" folder using an IDE or by using
the Java command.